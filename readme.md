# Credits
Growth Engineer Test for [Casumo](http://casumo.com/) by [Manuel Fernandez Lara](mailto:manuelflara@gmail.com).

# Stack
Even though it was advised to use Java & Spring for the project, I don't have experience with stack so given the scope of the project, it would've taken too long to use that. I used the Laravel framework (PHP & MySQL) instead, with Bootstrap and FushionCharts in the frontend.

# Live Demo
For your convenience, specially considering I didn't use your stack, I've deployed the project on the following URL: [http://flara.co](http://flara.co)

# Installation
If you want to run the source code locally, though, here are the instructions:

* Clone the [repo of the project](https://bitbucket.org/manuelflara/casumo.git).
* Use Laravel's [Homestead](https://laravel.com/docs/5.3/homestead) as a Vagrant box to run the project.
* SSH into Vagrant once configured and `cd` into the project directory
* run `composer install`
* run `install -g yarn`
* run `yarn`
* run `gulp`
* run `php artisan migrate`
* run `php artisan db:seed`

Note that this last last step (seeding the database with fake user data) will take a long time, so I suggest you check it after a few thousand have been created (you'll see a message in the screen every 1,000 users).

If there are any errors during the process, make sure that the project's .env file correctly reflects Vagrant's MySQL credentails, that a database named `casumo` exists there (otherwise create it) and that APP_URL is correct too (I personally map Vagrant's IP to a virtual host and use that value). Or ask me, of course :)

# Usage
There are two sections on the tool, which you can access from the top nav bar: Dashboard (opened by default) and Users.

### Dashboard
In the dashboard we see a few key metrics of our dataset, including growth rate (this metric the last 30 days compared to the previous 30 days) where it makes sense.

If you click on each metric you'll see a related chart for deeper insights.

Some metrics might look a bit odd to you, read the section "Weird looking metrics" below.

### Users
In this section you can see all the users in the database in a table layout. Features include:
* Search by user id or name (using LIKE).
* Filter by user segment
* Filter by user signup date (a few ranges available)
* Pagination
* Sorting by every displayed attribute (click again to switch ordering direction).

The `Spent` field is basically deposits - withdrawals - balance. This tells us how much money each user has actually spent (current balance doesn't count as spent yet, as it can be withdrawn at any time) or won (if the number is negative) on the service.

# Notes
### Key metrics missing
ARPU or CLTV are notably missing, since I don't really have info on how much money per user the company made. We can know how much they spent, but I assume some of that goes to other players while playing something like poker or blackjack and the company only takes like a participation fee, and not that all money missing from the total of deposits - withdrawals - balance is company revenue. If that's the case, obviously some important metrics could have been easily added to the dashboard.

Also, because I don't have a historic of bets placed and their results, as well as when withdrawals and deposits were made, a bunch of useful visualizations can't be produced from this dataset.

### Weird looking metrics
Since a fake and kinda randomly generated dataset was used and not real data, some metrics won't make much sense or look real at all. User sign up and last action dates, most importantly, have been quite evenly spread out over the last 3 years. This means things like engagement (which I calculated as percentage of users that have made an action in the last week) is incredibly low, when in a healthy business it should be way higher, maybe 20-60% of active users or something like that. Also, you'll see the chart for engagement rate looks a bit funny the last few days, *that's because I generated most of the dataset a few days ago*.

For things like bets placed, deposits and withdrawals per segment; % of users per segment, etc. I just made up some numbers that made sense to me, but way be way off compared to the real metrics of your business.

Would've been nice to have a realistic dataset :)

### Calculation of metrics
Some metrics aren't prooperly calculated for the sake of simplicity. For example, we can't really know the correct engagement rate since we don't know if a user that did something (updated_at) the last week *also* did something the week before, or hasn't been active in a year. Two users that did something today and signed up a year ago aren't equally engaged if one of them does an action every hour and the other once a month. But this is the data structure I have, so I approximated.

Another metric that is a bit half assed is converison rate. It only counts how many users made a deposit AND signed up in the last 30 days. Can't know who made a deposit in the last 30 days but signed up before, unfortunately.

### Performance
If we had x10 - x100 users we may have to optimize certain parts of the backend so it wouldn't be too slow, but as it is right now it works fine in a $5/month DigitalOcean droplet.

### Churn rate
I used a very simplistic way of calculating churn rate: total users churned over the last 30 days divided by (total active users today plus users churned during last 30 days). [There are more complex and maybe accurate ways of calculating this](https://blog.recurly.com/2014/08/better-way-to-calculate-your-churn-rate).

Also, since "CHURNED" on itself is a segment as per the project specs, and not its own separate field, I have no way of knowing (and showing) churn rate per user segment (normal, vip, etc).

### Caching
Metrics are cached in two ways in the Dashboard: once, in memory, since some are reused a few times during the execution thread. I added that before I added the second layer, now probably to simplify the code a bit it could be removed since layer two is fast enough considering how few times they get reused in the same thread. The second layer is the database. Metrics are stored once per day in the `metrics` table. This means that after the first visit of the day, you'll get a much faster response time (but the same value even if the underlying data changes). A side bonus of this is that, over time, we'd be able to easily chart changes over time for all metrics.