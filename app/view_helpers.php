<?php

use Carbon\Carbon;

/**
 * Data formatting helpers for Blade views.
 */

function metric_growth_color($metric) {
    if ($metric['growth'] < 0) {
        if (isset($metric['lowest_is_best'])) {
            return 'up';
        } else {
            return 'down';
        }
    } else {
        if (isset($metric['lowest_is_best'])) {
            return 'down';
        } else {
            return 'up';
        }
    }
}

function format_metric($metric) {
    $string = '';
    if (isset($metric['is_money'])) {
        $string .= '€';
    }

    if (isset($metric['is_percent'])) {
    }
    if ($metric['value'] > 1000000) {
        $string .= number_format($metric['value'] / 1000000, 1).'M';
    } elseif ($metric['value'] > 100000) {
        $string .= number_format($metric['value'] / 1000, 1).'K';
    } elseif (isset($metric['is_percent'])) {
        $string .= number_format($metric['value']*100, 2).'%';
    } else {
        $string .= number_format($metric['value'], isset($metric['decimals']) ? $metric['decimals'] : 0);
    }

    return $string;
}

function now() {
    return Carbon::now();
}