<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain\User\UserRepository;
use App\Domain\Metric\MetricRepository;

class MetricController extends Controller
{
    protected $userRepository;
    protected $metricRepository;

    public function __construct(UserRepository $userRepository, MetricRepository $metricRepository)
    {
        $this->userRepository = $userRepository;
        $this->metricRepository = $metricRepository;
    }

    /**
     * Show Dashboard page.
     */
    public function dashboard(Request $request)
    {
        return view('metrics.dashboard', ['metrics' => $this->getMetrics()]);
    }

    protected function getMetrics()
    {
        $metrics = config('metrics');

        $values = [];
        foreach ($metrics as $key => $metric) {
            $value = $this->metricRepository->calculateMetric($key, $metric);
            if ($value) {
                $values[] = $value;
            }
        }

        return $values;
    }
}