<?php

namespace App\Http\Controllers;

use App\Domain\User\Segment;
use App\Domain\Chart\ChartRepository;
use Illuminate\Http\Request;
use ConsoleTVs\Charts\Facades\Charts;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ChartController extends Controller
{
    protected $chartRepository;

    public function __construct(ChartRepository $chartRepository)
    {
        $this->chartRepository = $chartRepository;
    }

    public function showChart($metric)
    {
        $metrics = config('metrics');
        $method = $this->getMethodFromChartKey($metric);
        if (!method_exists($this, $method)) {
            throw new NotFoundHttpException("Chart $metric doesn't exist.");
        }

        return view('metrics.chart', [
            'chart' => $this->$method(),
            'title' => $metrics[$metric]['graph'],
        ]);
    }

    protected function getChurnRateChart()
    {
        $data = $this->chartRepository->getChurnRateData();

        return $this->getBarChart($data);
    }

    protected function getConversionRateChart()
    {
        $data = $this->chartRepository->getConversionRateData();

        return $this->getLineChart($data);
    }

    protected function getTotalEngagementChart()
    {
        $data = $this->chartRepository->getTotalEngagementData();

        return $this->getBarChart($data);
    }

    protected function getGrowthRateChart()
    {
        $data = $this->chartRepository->getGrowthRateData();

        return $this->getBarChart($data);
    }

    protected function getNewUsersThisMonthChart()
    {
        $data = $this->chartRepository->getNewUsersThisMonthData();

        return $this->getBarChart($data);
    }

    protected function getTotalActiveUsersChart()
    {
        $data = $this->chartRepository->getTotalActiveUsersData();

        return $this->getBarChart($data);
    }

    protected function getTotalBetsChart()
    {
        return $this->getTotalPieChart('bets');
    }

    protected function getTotalWithdrawalsChart()
    {
        return $this->getTotalPieChart('withdrawals');
    }

    protected function getTotalDepositsChart()
    {
        return $this->getTotalPieChart('deposits');
    }

    protected function getTotalBalanceChart()
    {
        return $this->getTotalPieChart('balance');
    }

    protected function getAvgBetsChart()
    {
        return $this->getAvgPieChart('bets');
    }

    protected function getAvgWithdrawalsChart()
    {
        return $this->getAvgPieChart('withdrawals');
    }

    protected function getAvgDepositsChart()
    {
        return $this->getAvgPieChart('deposits');
    }

    protected function getAvgBalanceChart()
    {
        return $this->getAvgPieChart('balance');
    }

    protected function getTotalPieChart($field)
    {
        $data = $this->chartRepository->getTotalPieData($field);

        return $this->getPieChart($data);
    }

    protected function getAvgPieChart($field)
    {
        $data = $this->chartRepository->getAvgPieData($field);

        return $this->getPieChart($data);
    }

    protected function getPieChart($data)
    {
        return $this->getChart('pie', $data);
    }

    protected function getBarChart($data)
    {
        return $this->getChart('bar', $data);
    }

    protected function getLineChart($data)
    {
        return $this->getChart('line', $data);
    }

    protected function getChart($type, $data)
    {
        $chart = Charts::create($type)
            ->labels($data['labels'])
            ->values($data['values']);

        return $chart;
    }


    protected function getMethodFromChartKey($key)
    {
        return 'get'.str_replace(' ', '', ucwords(str_replace('_', ' ', $key))).'Chart';
    }
}