<?php

namespace App\Http\Controllers;

use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Helpers\UrlBuilder;
use App\Domain\User\Segment;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $filter = [
            'search' => $request->input('search'),
            'segment' => $request->input('segment'),
            'age' => $request->input('age'),
            'sort' => $sort = $request->input('sort', 'id'),
            'order' => $request->input('order', $this->getDefaultSortOrder($sort)),
        ];

        $url = new UrlBuilder($request);

        return view('user.index', [
            'title' => $this->getIndexTitle($filter),
            'users' => $this->userRepository->getFilteredList($filter),
            'url' => $url,
            'columns' => $this->getTableColumns($filter, $url),
            'filter' => $filter,
        ]);
    }

    protected function getIndexTitle($filter)
    {
        $title = 'Users';

        if (!empty($filter['segment'])) {
            switch ($filter['segment']) {
                case Segment::NON_DEPOSITOR:
                    $title = 'Non Dep. '.$title;
                    break;
                case Segment::NORMAL:
                    $title = 'Normal '.$title;
                    break;
                case Segment::VIP:
                    $title = 'VIP '.$title;
                    break;
                case Segment::VERY_VIP:
                    $title = 'Very VIP '.$title;
                    break;
                case Segment::CHURNED:
                    $title = 'Churned '.$title;
                    break;
            }
        }

        if (!empty($filter['age'])) {
            $title .= ' <em>who joined in the last '.$filter['age'].' days</em>';
        }

        if (!empty($filter['search'])) {
            if (is_numeric($filter['search'])) {
                $title .= ' <strong>with ID #'.$filter['search'].'</strong>';
            } else {
                $title .= ' <strong>with a name like "'.$filter['search'].'"</strong>';
            }
        }

        return $title;
    }

    protected function getTableColumns($filter, $url)
    {
        $values = $this->getTableColumnKeys();

        $cols = [];
        foreach ($values as $key => $label) {
            $is_active = $filter['sort'] == $key;
            $cols[] = [
                'key' => $key,
                'label' => $label,
                'is_active' => $is_active,
                'icon' => $is_active ? 'sort-'.$filter['order'] : 'sort',
                'url' => $url->with(['sort' => $key, 'page' => 1, 'order' => $this->getSortOrderLinkParam($key, $filter)]),
            ];
        }

        return $cols;
    }

    protected function getTableColumnKeys()
    {
        return [
            'id' => 'Id',
            'segment' => 'Segment',
            'first_name' => 'Full Name',
            'created_at' => 'Joined',
            'bets' => 'Bets Placed',
            'deposits' => 'Deposits',
            'withdrawals' => 'Withdrawals',
            'balance' => 'Current Balance',
            'spent' => 'Spent',
        ];
    }

    protected function getSortOrderLinkParam($sort, $filter)
    {
        if ($filter['sort'] == $sort) {
            return 'asc' == $filter['order'] ? 'desc' : 'asc';
        }

        return $this->getDefaultSortOrder($sort);
    }

    protected function getDefaultSortOrder($sort)
    {
        switch ($sort) {
            case 'first_name':
            case 'id':
                return 'asc';
        }

        return 'desc';
    }
}
