<?php

namespace App\Domain\User;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'password', 'segment', 'deposits', 'bets', 'withdrawals', 'balance', 'is_churned',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'is_churned' => 'boolean',
    ];

    public function getSpentAttribute()
    {
        return $this->deposits - $this->withdrawals - $this->balance;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name .' '. $this->last_name;
    }

    public function getSegmentAbbrAttribute()
    {
        switch ($this->segment) {
            case Segment::NON_DEPOSITOR:    return 'NON.DEP';
            case Segment::NORMAL:           return 'NORMAL';
            case Segment::VIP:              return 'VIP';
            case Segment::VERY_VIP:         return 'V.VIP';
            case Segment::CHURNED:          return 'CHURNED';
        }

        return '';
    }

    public function getCurrencySymbolAttribute()
    {
        switch ($this->currency) {
            case 'EUR':    return '&euro;';
            case 'USD':    return '&usd;';
        }

        return '';
    }
}
