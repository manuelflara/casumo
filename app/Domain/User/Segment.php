<?php

namespace App\Domain\User;

class Segment
{
    const NON_DEPOSITOR = 'non_depositor';
    const NORMAL = 'normal';
    const VIP = 'vip';
    const VERY_VIP = 'very_vip';
    const CHURNED = 'churned';

    public static function getSegments()
    {
        return [
            Segment::NON_DEPOSITOR,
            Segment::NORMAL,
            Segment::VIP,
            Segment::VERY_VIP,
            Segment::CHURNED,
        ];
    }

    public static function keyToName($key)
    {
        return ucwords(str_replace('_', ' ', $key));
    }
}
