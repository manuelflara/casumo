<?php

namespace App\Domain\User;

use App\Domain\User\User;
use Illuminate\Support\Facades\DB;

class UserRepository
{
    CONST USERS_PER_PAGE = 20;

    public function getFilteredList($filter)
    {
        $query = User::selectRaw('users.*, (deposits - withdrawals - balance) AS spent')
            ->orderBy($filter['sort'], $filter['order']);

        if (!empty($filter['segment'])) {
            $query = $query->where('segment', $filter['segment']);
        }

        if (!empty($filter['search'])) {
            $search = $filter['search'];
            $query->where(function ($query) use ($search) {
                $query
                    ->where('users.first_name', 'LIKE', "%$search%")
                    ->orWhere('users.last_name', 'LIKE', "%$search%")
                    ->orWhere('users.id', '=', $search)
                ;
            });
        }

        if (!empty($filter['age'])) {
            $days = intval($filter['age']);
            $query = $query->whereRaw("created_at > NOW() - INTERVAL $days DAY");
        }

        return $query->paginate(self::USERS_PER_PAGE);
    }
}
