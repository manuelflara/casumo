<?php

namespace App\Domain\Metric;

use Illuminate\Database\Eloquent\Model;

/**
 * This model represents a certain metric (`key`) calculated in time (`created_at`, which is a DATE column); for historical & caching purposes.
 */
class Metric extends Model
{
    protected $fillable = [
        'key', 'created_at', 'value', 'growth',
    ];

    protected $casts = [
        'created_at' => 'date',
    ];
}
