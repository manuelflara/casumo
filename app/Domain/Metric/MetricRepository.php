<?php

namespace App\Domain\Metric;

use Carbon\Carbon;
use App\Domain\User\Segment;
use Illuminate\Support\Facades\DB;

class MetricRepository
{
    private $metrics_cache = [];

    public function calculateMetric($key, $args = null)
    {
        // First check if metric has already been calculated in this exection thread or today (is stored in database).
        $metric = $this->retrieveMetric($key);
        if ($metric) {
            return $this->prepareMetric($metric, $key, $args);
        }

        $method = $this->getMetricMethodFromKey($key);
        if (!method_exists($this, $method)) {
            throw new \Exception("Trying to calculate unknown metric $key");
        }

        $metric = $this->$method();

        $this->metrics_cache[$key] = $metric;
        $metric = $this->prepareMetric($metric, $key, $args);

        // Store in database.
        $this->storeMetric($metric);

        return $metric;
    }

    private function storeMetric($metric)
    {
        $data = [
            'key' => $metric['key'],
            'created_at' => Carbon::today()->toDateString(),
            'value' => $metric['value'],
        ];

        if (isset($metric['growth'])) {
            $data['growth'] = $metric['growth'];
        }

        DB::table('metrics')->insert($data);
    }

    private function retrieveMetric($key)
    {
        if (isset($this->metrics_cache[$key])) {
            return $this->metrics_cache[$key];
        }

        $metric = $this->getMetricFromDatabase($key);
        if ($metric) {
            $this->metrics_cache[$key] = [
                'value' => $metric->value,
                'growth' => $metric->growth,
            ];

            return $this->metrics_cache[$key];
        }

        return false;
    }

    private function getMetricFromDatabase($key)
    {
        return DB::table('metrics')
            ->where('key', $key)
            ->whereDate('created_at', '=', Carbon::today()->toDateString())
            ->first();
    }

    private function prepareMetric($metric, $key, $args)
    {
        $metric['key'] = $key;
        if (!$args) {
            return $metric;
        }

        return array_merge($args, $metric);
    }

    private function getMetricMethodFromKey($key)
    {
        return 'calculate'.str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
    }

    protected function calculateNewUsersThisMonth()
    {
        $new_users_this_month = DB::table('users')
            ->whereBetween('created_at', [now()->subMonth(), now()])
            ->count();
        $new_users_prev_month = DB::table('users')
            ->whereBetween('created_at', [now()->subMonths(2), now()->subMonth()])
            ->count();

        return [
            'value' => $new_users_this_month,
            'growth' => $this->calculateGrowth($new_users_this_month, $new_users_prev_month),
        ];
    }

    protected function calculateTotalActiveUsers()
    {
        return [
            'value' => DB::table('users')->where('is_churned', false)->count(),
        ];
    }

    protected function calculateTotalChurn()
    {
        $total_active_users = $this->calculateMetric('total_active_users')['value'];
        $total_churned_users = DB::table('users')->where('is_churned', true)->count();

        return [
            'value' => $this->safeDivision($total_churned_users, $total_active_users),
        ];
    }

    protected function calculateConversionRate()
    {
        $query = DB::table('users')->whereBetween('created_at', [now()->subMonth(), now()]);
        $new_users_this_month = $query->count();
        $users_deposited_this_month = $query->where('deposits', '>', 0)->count();

        $query = DB::table('users')->whereBetween('created_at', [now()->subMonths(2), now()->subMonth()]);
        $new_users_prev_month = $query->count();
        $users_deposited_prev_month = $query->where('deposits', '>', 0)->count();

        $conversion_rate = $this->safeDivision($users_deposited_this_month, $new_users_this_month);
        $conversion_rate_prev = $this->safeDivision($users_deposited_prev_month, $new_users_prev_month);

        return [
            'value' => $conversion_rate,
            'growth' => $this->calculateGrowth($conversion_rate, $conversion_rate_prev),
        ];
    }

    protected function calculateGrowthRate()
    {
        // Last month.
        $total_active_users = $this->calculateMetric('total_active_users')['value'];
        $total_active_users_30d_ago = DB::table('users')
            ->where('is_churned', false)
            ->where('created_at', '<', now()->subMonth())
            ->count() ;
        $churned_users_this_month_but_active_last = DB::table('users')
            ->where('is_churned', true)
            ->where('updated_at', '>', now()->subMonth())
            ->where('created_at', '<', now()->subMonth())
            ->count();

        $total_active_users_prev = ($total_active_users_30d_ago + $churned_users_this_month_but_active_last);
        $growth_rate = $this->calculateGrowth($total_active_users, $total_active_users_prev);

        // Prev month.
        $total_active_users_60d_ago = DB::table('users')
            ->where('is_churned', false)
            ->where('created_at', '<', now()->subMonths(2))
            ->count() ;
        $churned_users_prev_month_but_active_last = DB::table('users')
            ->where('is_churned', true)
            ->whereBetween('updated_at', [now()->subMonths(2), now()->subMonth()])
            ->where('created_at', '<', now()->subMonths(2))
            ->count();

        $growth_rate_prev = $this->calculateGrowth($total_active_users_prev, $total_active_users_60d_ago + $churned_users_prev_month_but_active_last);

        return [
            'value' => $growth_rate,
            'growth' => $this->calculateGrowth($growth_rate, $growth_rate_prev),
        ];
    }

    protected function calculateChurnRate()
    {
        // Last month.
        $total_active_users = $this->calculateMetric('total_active_users')['value'];
        $churned_users = DB::table('users')
            ->where('is_churned', true)
            ->whereBetween('updated_at', [now()->subMonth(), now()])
            ->count();
        $churn_rate = $this->safeDivision($churned_users, $total_active_users + $churned_users);

        // Prev month.
        $total_active_users_prev = DB::table('users')
            ->where('is_churned', false)
            ->where('created_at', '<', now()->subMonth())
            ->count() + $churned_users;
        $churned_users_prev = DB::table('users')
            ->where('is_churned', true)
            ->whereBetween('updated_at', [now()->subMonths(2), now()->subMonth()])
            ->count();
        $churn_rate_prev = $this->safeDivision($churned_users_prev, $total_active_users_prev + $churned_users_prev);

        return [
            'value' => $churn_rate,
            'growth' => $this->calculateGrowth($churn_rate, $churn_rate_prev),
        ];
    }

    protected function calculateTotalEngagement()
    {
        $total_active_users = $this->calculateMetric('total_active_users')['value'];
        $total_users_active_last_week = DB::table('users')
            ->where('is_churned', false)
            ->whereBetween('updated_at', [now()->subWeek(), now()])
            ->count();
        $total_users_active_prev_week = DB::table('users')
            ->where('is_churned', false)
            ->whereBetween('updated_at', [now()->subWeeks(2), now()->subWeek()])
            ->count();
        $total_users_signedup_last_week = DB::table('users')
            ->where('is_churned', false)
            ->whereBetween('created_at', [now()->subWeek(), now()])
            ->count();

        $total_engagement = $this->safeDivision($total_users_active_last_week, $total_active_users);

        $total_engagement_prev = $this->safeDivision($total_users_active_prev_week, $total_active_users - $total_users_signedup_last_week);

        return [
            'value' => $total_engagement,
            'growth' => $this->calculateGrowth($total_engagement, $total_engagement_prev),
        ];
    }

    protected function calculateTotalBets()
    {
        return $this->calculateFieldSum('bets');
    }

    protected function calculateTotalDeposits()
    {
        return $this->calculateFieldSum('deposits');
    }

    protected function calculateTotalWithdrawals()
    {
        return $this->calculateFieldSum('withdrawals');
    }

    protected function calculateTotalBalance()
    {
        return $this->calculateFieldSum('balance');
    }

    protected function calculateAvgBets()
    {
        return $this->calculateFieldAvg('bets');
    }

    protected function calculateAvgDeposits()
    {
        return $this->calculateFieldAvg('deposits');
    }

    protected function calculateAvgWithdrawals()
    {
        return $this->calculateFieldAvg('withdrawals');
    }

    protected function calculateAvgBalance()
    {
        return $this->calculateFieldAvg('balance');
    }

    protected function calculateFieldSum($field)
    {
        return [
            'value' => DB::table('users')->sum($field),
        ];
    }

    protected function calculateFieldAvg($field)
    {
        return [
            'value' => DB::table('users')->where('segment', '<>', Segment::NON_DEPOSITOR)->avg($field),
        ];
    }

    protected function calculateGrowth($now, $prev)
    {
        if (0 == $prev) {
            return 0 == $now ? 0 : 1;
        }

        return $now / $prev - 1;
    }

    protected function safeDivision($a, $b)
    {
        if (0 == $b) {
            return 0;
        }

        return $a / $b;
    }
}
