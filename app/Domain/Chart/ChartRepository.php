<?php

namespace App\Domain\Chart;

use Carbon\Carbon;
use App\Domain\User\User;
use App\Domain\User\Segment;
use Illuminate\Support\Facades\DB;

class ChartRepository
{
    public function getChurnRateData()
    {
        return $this->getUsersPerMonthData(false, true);
    }

    public function getNewUsersThisMonthData()
    {
        return $this->getUsersPerMonthData(false);
    }

    public function getTotalActiveUsersData()
    {
        return $this->getUsersPerMonthData(true, false);
    }

    public function getConversionRateData()
    {
        return $this->getUsersPerMonthData(false, null, "(SUM(IF(deposits > 0, 1, 0)) / COUNT(*) * 100) AS total");
    }

    public function getGrowthRateData()
    {
        return $this->getUsersPerMonthData(false, null, "SUM(IF(is_churned = 0, 1, 0)) AS total");
    }

    public function getTotalEngagementData()
    {
        $query = DB::table('users')
            ->selectRaw('COUNT(*) AS total, (TO_DAYS(NOW()) - TO_DAYS(updated_at)) AS day_last_action')
            ->groupBy('day_last_action')
            ->orderBy('day_last_action', 'ASC')
            ->take(60);

        $rows = $query->get()->reverse();

        $data = [
            'labels' => [],
            'values' => [],
        ];
        $accum = 0;
        foreach ($rows as $row) {
            $data['labels'][] = 0 == $row->day_last_action ? 'Last 24 hours' : 'Day -'.$row->day_last_action;
            $data['values'][] = $row->total;
        }

        $data['labels'] = $data['labels'];
        $data['values'] = $data['values'];

        return $data;
    }

    protected function getUsersPerMonthData($accummulate, $is_churned = null, $select = null)
    {
        if (!$select) {
            $select = 'COUNT(*) AS total';
        }
        $query = DB::table('users')
            ->selectRaw($select . ', YEAR(created_at) AS y, MONTH(created_at) AS m')
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
            ->orderBy(DB::raw('YEAR(created_at) DESC, MONTH(created_at) '), 'DESC');

        if (null !== $is_churned) {
            $query = $query->where('is_churned', $is_churned);
        }

        // Since we want last month last in the chart...
        $rows = $query->get()->reverse();

        $data = [
            'labels' => [],
            'values' => [],
        ];
        $accum = 0;
        foreach ($rows as $row) {
            $data['labels'][] = $this->getMonthName($row->m).' '.$row->y;
            $data['values'][] = $row->total + $accum;

            if ($accummulate) {
                $accum += $row->total;
            }
        }

        // Since we only want the last 12 months.
        $data['labels'] = array_slice($data['labels'], -12);
        $data['values'] = array_slice($data['values'], -12);

        return $data;
    }

    public function getTotalPieData($field)
    {
        $rows = DB::table('users')
            ->selectRaw("sum($field) AS total, segment")
            ->groupBy('segment')
            ->get();

        return $this->getPieData($rows);
    }

    public function getAvgPieData($field)
    {
        $rows = DB::table('users')
            ->selectRaw("avg($field) AS total, segment")
            ->groupBy('segment')
            ->get();

        return $this->getPieData($rows);
    }

    protected function getPieData($rows)
    {
        $data = [
            'labels' => [],
            'values' => [],
        ];
        foreach ($rows as $row) {
            $data['labels'][] = Segment::keyToName($row->segment);
            $data['values'][] = round($row->total, 2);
        }

        return $data;
    }

    protected function getMonthName($monthNum)
    {
        return date('F', mktime(0, 0, 0, $monthNum, 10));
    }
}
