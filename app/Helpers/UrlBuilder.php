<?php

namespace App\Helpers;

use Illuminate\Http\Request;

/**
 * Class created to easily create different links for similar URLs on the same page.
 * Example: a list of content (users, etc) that will have lots of links on the same
 * page to itself, including all current params (order, sort, filters...) but changing
 * one of them: each link in the pagination, each table column header, etc. has the
 * same URL but changing a part of it. This makes it easy to do in the view.
 */
class UrlBuilder
{
    private $base_url;
    private $params;

    /**
     * Will either use ->input params if passed a Request, or a given array of
     * key=>values. Similarly, if $base_url is null and a Request is given,
     * it will use current path as base url.
     */
    public function __construct($params, $base_url = null)
    {
        $this->params = $params;
        $this->base_url = $base_url;

        if ($params instanceof Request) {
            $this->params = $params->input();
            $this->base_url = $base_url ? $base_url : $params->getPathInfo();
        }
    }

    /**
     * Builds URL with $value as $param's value instead of current page's value.
     */
    public function with($param, $value = null)
    {
        $params = $this->params;

        /*
         * Can also be called to override several params like ->with([sort=>id,order=>asc])
         */
        if (is_array($param)) {
            $params = array_merge($params, $param);
        } elseif (null === $value) {
            return $this->without($param);
        } else {
            $params[$param] = $value;
        }

        return $this->get($params);
    }

    /**
     * Builds URL removing $param from list of current params.
     * Accepts either one param or an array of params as input.
     */
    public function without($remove_params)
    {
        $params = $this->params;
        if (!is_array($remove_params)) {
            $remove_params = [$remove_params];
        }

        $params = array_diff_key($params, array_flip($remove_params));

        return $this->get($params);
    }

    /**
     * Return base url without any params in the query string.
     */
    public function path()
    {
        return $this->get([], '');
    }

    /**
     * Can be called as is to just get a link to the current page with current params.
     */
    public function get($params = null, $endIfEmpty = '?')
    {
        $params = ($params !== null) ? $params : $this->params;
        if (empty($params)) {
            return $this->base_url.$endIfEmpty;
        }
        /*
         * Remove params with empty value. Custom comparison since
         * we don't want to exxclude params with value=0
         */
        $params = array_filter($params, function ($var) {
            return $var !== null && $var !== false && $var !== '';
        });
        $query = implode(
            '&',
            array_map(
                function ($v, $k) { return "$k=$v"; },
                $params,
                array_keys($params)
            )
        );

        return $this->base_url.'?'.$query;
    }
}
