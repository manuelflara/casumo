<?php

return [

    'growth_rate' => [
        'title' => 'Growth Rate',
        'explanation' => 'Net users gained last month.',
        'size' => 4, // Size of the bootstrap badge in the HTML.
        'graph' => 'Net Users Gained Per Month',
        'is_percent' => true,
        'decimals' => 2,
    ],

    'churn_rate' => [
        'title' => 'Churn Rate',
        'explanation' => 'Users that cancelled last month.',
        'size' => 4,
        'graph' => 'Users That Churned Per Month',
        'is_percent' => true,
        'lowest_is_best' => true,
    ],

    'conversion_rate' => [
        'title' => 'Conversion Rate',
        'explanation' => 'Users who made a deposit after signup.',
        'graph' => 'Users Who Signed Up & Made A Deposit Per Month (in %)',
        'size' => 4,
        'is_percent' => true,
    ],

    'total_engagement' => [
        'title' => 'Engagement Rate',
        'explanation' => 'Users who performed an action last week.',
        'size' => 4,
        'graph' => 'Users Last Action Per Day (in %)',
        'is_percent' => true,
        'period' => '7 days',
    ],

    'new_users_this_month' => [
        'title' => 'New Signups This Month',
        'size' => 4, // Size of the bootstrap badge in the HTML.
        'graph' => 'New Signups Per Month',
    ],

    'total_active_users' => [
        'title' => 'Total Active Users',
        'size' => 4,
        'graph' => 'Total Active Users Per Month',
    ],

    'total_bets' => [
        'title' => 'Total Bets Placed',
        'size' => 3,
        'graph' => 'Total Bets Placed Per User Segment',
    ],

    'total_deposits' => [
        'title' => 'Total Deposits',
        'size' => 3,
        'graph' => 'Total Deposits Made Per User Segment (in €)',
        'is_money' => true,
    ],

    'total_withdrawals' => [
        'title' => 'Total Withdrawals',
        'size' => 3,
        'graph' => 'Total Withdrawals Made Per User Segment (in €)',
        'is_money' => true,
    ],

    'total_balance' => [
        'title' => 'Total in Balance',
        'size' => 3,
        'graph' => 'Total in Balance Per User Segment (in €)',
        'is_money' => true,
    ],

    'avg_bets' => [
        'title' => 'Avg. Bets Per User',
        'explanation' => 'Excluding non depositors.',
        'size' => 3,
        'graph' => 'Average Bets Placed Per User Segment',
        'decimals' => 1,
    ],

    'avg_deposits' => [
        'title' => 'Avg. Deposited Per User',
        'explanation' => 'Excluding non depositors.',
        'size' => 3,
        'graph' => 'Average Deposited Per User Segment (in €)',
        'is_money' => true,
        'decimals' => 1,
    ],

    'avg_withdrawals' => [
        'title' => 'Avg. Withdrawn Per User',
        'explanation' => 'Excluding non depositors.',
        'size' => 3,
        'graph' => 'Average Withdrawn Per User Segment (in €)',
        'is_money' => true,
        'decimals' => 1,
    ],

    'avg_balance' => [
        'title' => 'Avg. in Balance Per User',
        'explanation' => 'Excluding non depositors.',
        'size' => 3,
        'graph' => 'Average Current Balance Per User Segment (in €)',
        'is_money' => true,
        'decimals' => 1,
    ],

];
