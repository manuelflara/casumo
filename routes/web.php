<?php

Route::get('/', 'MetricController@dashboard');
Route::get('metric/{metric}', 'ChartController@showChart');
Route::get('users', 'UserController@index');
