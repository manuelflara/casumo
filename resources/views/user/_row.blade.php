@if ('churned' === $user->segment)
<tr class="danger">
@else
<tr>
@endif
  <td>{{ $user->id }}</td>
  <td>{{ $user->segment_abbr }}</td>
  <td class="main">{{ $user->full_name }}</td>
  <td class="date"><acronym title="{{ $user->created_at }}">{{ $user->created_at->diffForHumans() }}</acronym></td>
  <td class="numeric">{{ number_format($user->bets) }}</td>
  <td class="numeric">{!! $user->currency_symbol !!} {{ number_format($user->deposits, 2) }}</td>
  <td class="numeric">{!! $user->currency_symbol !!} {{ number_format($user->withdrawals, 2) }}</td>
  <td class="numeric">{!! $user->currency_symbol !!} {{ number_format($user->balance, 2) }}</td>
  <td class="numeric">{!! $user->currency_symbol !!} {{ number_format($user->spent, 2) }}</td>
</tr>