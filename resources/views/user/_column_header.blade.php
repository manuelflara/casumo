<th class="{{$column['key']}} sort {{$column['is_active']?'active':''}}">
  <a href="{{$column['url']}}">{{$column['label']}}<i class="fa fa-{{$column['icon']}} fa-lg"></i></a>
</th>