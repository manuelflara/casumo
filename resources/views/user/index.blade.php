@extends('layout')

@section('content')
<div class="col-md-12 users-page">
  <h1>{!! $title !!}</h1>
  <nav class="navbar navbar-filters">
    <p class="navbar-text">Filter by:</p>
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Segment <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{$url->with('segment', 'normal')}}">NORMAL</a></li>
          <li><a href="{{$url->with('segment', 'vip')}}">VIP</a></li>
          <li><a href="{{$url->with('segment', 'very_vip')}}">VERY VIP</a></li>
          <li><a href="{{$url->with('segment', 'churned')}}">CHURNED</a></li>
          <li><a href="{{$url->with('segment', 'non_depositor')}}">NON.DEP</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="{{$url->without('segment')}}">All users</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Join Date <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{$url->with('age', '30')}}">Joined last month</a></li>
          <li><a href="{{$url->with('age', '90')}}">Joined last 3 months</a></li>
          <li><a href="{{$url->with('age', '365')}}">Joined last year</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="{{$url->without('age')}}">All users</a></li>
        </ul>
      </li>
    </ul>
    <form class="navbar-form navbar-right" method="get" action="{{$url->path()}}">
      <div class="form-group">
        <input type="search" class="form-control" placeholder="Search by user id or name" autofocus name="search">
      </div>
    </form>
  </nav>
  <nav class="navbar navbar-filters">
    <p class="navbar-text navbar-right num_results">{{ number_format($users->total()) }} results</p>
    {{ $users->appends($filter)->links() }}
  </nav>

  @if (!$users->isEmpty())
    <table class="table table-users table-striped table-bordered table-hover">
      <thead>
        <tr>
          @each('user._column_header', $columns, 'column')
        </tr>
      </thead>
      <tbody>
        @each("user._row", $users, 'user')
      </tbody>
    </table>
  @else
     <div class="alert alert-danger" role="alert">No users match your criteria.</div>
  @endif
</div>
@endsection
