<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <link rel="icon" href="favicon.png" type="image/png">
  <link href="/css/app.css" rel="stylesheet">

  {!! Charts::assets() !!}
</head>
<body>
  <div id="app">
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">

          <!-- Collapsed Hamburger -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <!-- Branding Image -->
          <a class="navbar-brand" href="{{ url('/') }}">
            Casumo
          </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard fa-lg"></i>Dashboard</a></li>
            <li><a href="{{ url('/users') }}"><i class="fa fa-users fa-lg"></i>Users</a></li>
          </ul>
        </div>
      </div>
    </nav>

    @if (Session::has('message'))
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="alert alert-{{Session::get('message_type', 'success')}}" role="alert">{!! Session::get('message') !!}</div>
        </div>
      </div>
    </div>
    @endif

    <div class="container">
      <div class="row">
        @yield('content')
      </div>
    </div>
  </div>

  <script src="/js/app.js"></script>
  <script src="https://use.fontawesome.com/3c72de113b.js"></script>
</body>
</html>
