@extends('layout')

@section('content')
<div class="col-md-12 chart">
  <h5><a href="{{ url('/') }}">&laquo; Back to Dashboard</a></h5>
  <h1>{{ $title }}</h1>

  {!! $chart->render() !!}
</div>
@endsection