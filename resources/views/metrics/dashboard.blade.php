@extends('layout')

@section('content')
<div class="col-md-12 dashboard">
  <h1>Metrics Overview</h1>

  @each('metrics._badge', $metrics, 'metric')
</div>
@endsection
