<div class="col-md-{{ $metric['size'] }}">
  <a class="metric-badge" href="{{ url('/metric/'.$metric['key']) }}" title="{{ isset($metric['graph']) ? $metric['graph'] : '' }}">
    @if (isset($metric['growth']))
    <div class="growth {{ metric_growth_color($metric) }}">
      <i class="fa fa-arrow-{{ $metric['growth'] < 0 ? 'down' : 'up' }}"></i>
      {{ number_format(abs($metric['growth'] * 100), 1) }}%
      <small>{{ isset($metric['period']) ? $metric['period'] : '30 days' }} ago</small>
    </div>
    @endif
    <h5 class="title">{{ $metric['title'] }}</h5>
    <strong class="value">
      {{ format_metric($metric) }}
    </strong>
    @if (isset($metric['explanation']))
      <small class="explanation">{{ $metric['explanation'] }}</small>
    @endif
  </a>
</div>