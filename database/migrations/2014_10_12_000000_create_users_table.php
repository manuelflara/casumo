<?php

use App\Domain\User\User;
use App\Domain\User\Segment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('password');
            $table->enum('segment', Segment::getSegments())->default(Segment::NON_DEPOSITOR);
            $table->timestamps();
            $table->string('currency')->default('EUR');
            $table->float('deposits')->default(0);
            $table->integer('bets')->default(0);
            $table->float('withdrawals')->default(0);
            $table->float('balance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
