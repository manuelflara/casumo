<?php

use App\Domain\User\Segment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsChurnedToUsers extends Migration
{
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->boolean('is_churned')->default(false);
            $table->index('is_churned');
        });


        DB::table('users')->where('segment', Segment::CHURNED)->update(['is_churned' => true]);
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropIndex('is_churned');
            $table->dropColumn('is_churned');
        });
    }
}
