<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricsTable extends Migration
{
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->date('created_at');
            $table->double('value');
            $table->float('growth')->nullable();

            $table->unique(['key', 'created_at']);
        });
    }

    public function down()
    {
        Schema::drop('metrics');
    }
}
