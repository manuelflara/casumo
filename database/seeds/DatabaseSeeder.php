<?php

use Faker\Generator;
use Faker\Factory as Faker;
use App\Domain\User\Segment;
use Illuminate\Database\Seeder;
use Illuminate\Database\QueryException;

class DatabaseSeeder extends Seeder
{
    const NUM_USERS_TO_CREATE = 685494;

    /**
     * Seeds the <users> table with fake data.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->delete();
        $faker = Faker::create();
        $batch = [];
        $num_inserts_per_batch = 100;
        $num_inserts = 0;
        $num_starting_users = DB::table('users')->count();

        for ($i=$num_starting_users; $i<self::NUM_USERS_TO_CREATE; $i++) {
            $batch[] = $this->createFakeUserData($faker);

            if (0 === $i % $num_inserts_per_batch) {
                DB::table('users')->insert($batch);
                $num_inserts += $num_inserts_per_batch;
                $batch = [];

                if (0 === $num_inserts % 1000) {
                    echo 'Inserted ', $num_inserts, ' users', PHP_EOL;
                }
            }
        }

        if (!empty($batch)) {
            DB::table('users')->insert($batch);
        }
    }

    /**
     * Returns fake (but realistic looking) data for a user.
     *
     * @return array
     */
    private function createFakeUserData(Generator $faker)
    {
        $segment = $this->getRandomSegment();
        $metrics = $this->getFakeMetrics($faker, $segment);

        return $metrics + [
            'password' => bcrypt($faker->email),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'segment' => $segment,
            'is_churned' => Segment::CHURNED === $segment,
            'created_at' => $created_at = $faker->dateTimeBetween('-3 years', 'now'),
            'updated_at' => $faker->dateTimeBetween($created_at, 'now'),
        ];
    }

    /**
     * Returns fake (but realistic looking) metrics for a user, depending on its segment.
     *
     * @return array
     */
    private function getFakeMetrics(Generator $faker, $segment)
    {
        if (Segment::NON_DEPOSITOR === $segment) {
            return [
                'deposits' => 0,
                'bets' => 0,
                'withdrawals' => 0,
                'balance' => 0,
            ];
        }

        $loss_rate = $faker->randomFloat(2, 0.5, 0.75); // 50-75% of deposits get lost playing.
        $avg_bet_amount = $faker->randomFloat(2, 0.01, 1);

        $deposits = $faker->numberBetween(5, 100);
        $bets = ceil($deposits * 1.0 / $avg_bet_amount);
        $balance = $deposits * 0.1;
        $withdrawals = $loss_rate > 0.55 ? $faker->randomFloat(2, 10, $balance / 2) : 0;
        if ($balance < 0) {
            $balance = 0;
        }

        $multiplier = 1;
        if (Segment::VIP === $segment) {
            $multiplier = 10;
        } elseif (Segment::VERY_VIP === $segment) {
            $multiplier = 100;
        }

        // Lucky 5% winner?
        if ($faker->numberBetween(0, 100) <= 5) {
            $balance *= 5;
            $withdrawals *= 5;
        }

        // Who cancelled their account probably withdrew everything beforehand.
        if (Segment::CHURNED) {
            $withdrawals += $balance;
            $balance = 0;
        }

        return [
            'deposits' => $deposits * $multiplier,
            'bets' => $bets * $multiplier,
            'withdrawals' => $withdrawals * $multiplier,
            'balance' => $balance * $multiplier,
        ];
    }

    /**
     * Returns a random segment but weighted against the % we want of each kind.
     *
     * @return string App\Domain\User\Segment
     */
    private function getRandomSegment()
    {
        /**
         * Segment => % of total users we roughly aim to have in of this kind,
         * in order to realistically portray our fake user base. The remaining
         * (73% in this case) will be NORMAL.
         */
        $segments = [
            Segment::NON_DEPOSITOR => 10,
            Segment::CHURNED => 10,
            Segment::VERY_VIP => 2,
            Segment::VIP => 5,
        ];

        $num = rand(0, 100);
        $total_weight = 0;
        foreach ($segments as $segment => $weight) {
            $total_weight += $weight;

            if ($num <= $total_weight) {
                return $segment;
            }
        }

        return Segment::NORMAL;
    }
}
